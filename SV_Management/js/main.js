var dssv=[];
var dssvJson = localStorage.getItem("DSSV");
if(dssvJson != null){
  var svArr = JSON.parse(dssvJson);
  dssv = svArr.map(function(item){
  return new sinhVien(item.ma,item.ten,item.email,item.matKhau,item.diemToan,item.diemLy,item.diemHoa);
}
);
renderDSSV(dssv);
}
function themSinhVien(){
var sv = layThongTinTuForm();
dssv.push(sv);
var dssvJson = JSON.stringify(dssv);
localStorage.setItem("DSSV",dssvJson);
renderDSSV(dssv);
};

function xoaSinhVien(idSv){
var viTri = timKiemViTri(idSv,dssv);
if(viTri != -1){
dssv.splice(viTri,1);
renderDSSV(dssv);
}
}

function suaSinhVien(idSv){
  var viTri = timKiemViTri(idSv,dssv);
  if(viTri== -1){
    return;
};
var  sv = dssv[viTri];
showThongTinlenForm(sv);
}
function capNhatSinhVien(){
  var sv = layThongTinTuForm();
  var viTri = timKiemViTri(sv.ma,dssv);
  if(viTri!= -1){
    dssv[viTri] = sv;
    renderDSSV(dssv);
  }
}